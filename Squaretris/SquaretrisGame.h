#ifndef SQUARETRIS_GAME_H
#define SQUARETRIS_GAME_H

#include <string>

enum GameState {
    NORMAL,
    SPLASH,
    PAUSED,
    LOST,
    LOST_HIGHSCORE
};

enum AchievementState {
	LOCKED,
	NEWLY_UNLOCKED,
	UNLOCKED
};

enum AchievementNames {
	BEAT_LEVELS,
	GET_TOP_SCORE,
	CLEAR_4_ROWS,
	CLEAR_FULL_BOARD
};

// Decided to do this as some super low level stuff just for "fun".

const short tetrominos[7][4] = {{0x00F0, 0x4444, 0x0F00, 0x2222},  // I shape
                                {0x0066, 0x0066, 0x0066, 0x0066},  // O shape
                                {0x0071, 0x0226, 0x08E0, 0x0322},  // J shape
                                {0x0074, 0x0622, 0x0170, 0x0223},  // L shape
                                {0x0036, 0x0462, 0x0360, 0x0231},  // S shape
                                {0x0072, 0x0262, 0x0270, 0x0232},  // T shape
                                {0x0063, 0x0264, 0x0630, 0x0132}}; // Z shape

const unsigned long next_level_score[11] = {1000, 3000, 6000, 10000, 15000, 21000, 27000, 36000, 44000, 53000, 100000000};
const int level_drop_timers[11]          = {  40,   30,   20,    10,     9,     8,     7,     6,     5,     4,         2};
const int level_lock_timers[11]          = {  10,   10,    9,     9,     8,     8,     7,     7,     6,     6,         5};

const int BOARD_WIDTH = 10;
const int BOARD_HEIGHT = 20;
const int HIGH_SCORE_LIST_LENGTH = 10;

class SquaretrisGame {
public:
    // Public methods
    void reset();
    
    void moveLeft();
    void moveRight();
    void rotate();
    bool fall();
    void drop();
    bool pieceOnBottom();
    
    bool isHighscore();
    void addHighscore(const char* name);
    
    // Public data
    int board[BOARD_WIDTH][BOARD_HEIGHT];
    int next_piece;
    int current_piece;
    int piece_position_x;
    int piece_position_y;
    int piece_rotation;
    int lock_timer;
    int level;
    unsigned long score;
    unsigned int high_scores[HIGH_SCORE_LIST_LENGTH];
    char high_score_names[HIGH_SCORE_LIST_LENGTH][9];
	bool achievement_unlocked;
	int achievements[4];
    GameState state;

	// Drop speed is typically measured in units of 'G' where 
	// the block falls one row per frame for each G. A block with a
	// drop speed of 10G would fall 10 rows per frame, while a block
	// with a drop speed of 0.1G would fall one row every 10 frames.
	// See http://tetris.wikia.com/wiki/Drop for more details.
	// In Squaretris, drop_timer is the number of frames between
	// falls. (so higher = slower.)
	// G can be calculated by G = 1 / drop_timer.
	int drop_timer;
private:
    // Private methods
    bool valid();
    void clearRows();
};

#endif