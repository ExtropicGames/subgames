# Squaretris

It is the year 2024. The United Nations Radio Observatory has completed the Square Kilometer Array, the largest radio telescope ever built. As soon as the telescope was turned on, a message began streaming in. The message consisted of seven blocks of information, repeated over and over in a code that had never been heard before on Earth. As Earth’s top scientists began to decode the message, they realized it contained instructions for building a spacecraft that held technologies far beyond anything Earth science had yet achieved.

Your job, as a NASA technician, is to assist in decoding the message and discover the secrets of the technology locked within it.

## Compiling

- `brew instal sdl sdl_mixer sdl_image sdl_ttf`
- `cd Squaretris && make`

TicTacToe
=========

It's just Tic Tac Toe.
