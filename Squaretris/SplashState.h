#ifndef SPLASH_STATE_H
#define SPLASH_STATE_H

#ifdef WIN32
#include <SDL.h>
#include <SDL_mixer.h>
#else
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>
#endif

#include "StateMachine.h"

class SplashState : public State {
public:
    int id() { return 2; }

    void start();
    void finish();
    void update();
    void render();
private:
    SDL_Surface* screen;

    SDL_Surface* splash_image;
    SDL_Surface* splash_logo_image;
    SDL_Surface* splash_text_image;
    SDL_Surface* splash_background_image;

    Mix_Music* music;

    int slide_position_x;
    int slide_position_y;
    int slide_velocity_x;
    int slide_velocity_y;

    Uint32 prev_color;
    Uint32 next_color;
    int color_state;

    void rainbow();

    static const int FRAMES_PER_SECOND = 20;
};

#endif
