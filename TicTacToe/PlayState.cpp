#include <vector>
#include <fstream>

#include <cstdlib>
#include <ctime>

#include <SDL/SDL.h>

#include "PlayState.h"
#include "sdlfunc.h"

using namespace std;

void PlayState::start() {
    
    for (int i = 0; i < 3; i++) {
        for (int m = 0; m < 3; m++) {
            board[i][m] = 0;
        }
    }
    blank = loadImage("data/blank.png"); 
    assert(blank);
    x = loadImage("data/x.png");
    assert(x);
    o = loadImage("data/o.png");
    assert(o);
    win_image = loadImage("data/win.png");
    assert(win_image);
    lose_image = loadImage("data/lose.png");
    assert(lose_image);
    tie_image = loadImage("data/tie.png");
    assert(tie_image);
    board_image = loadImage("data/board.png");
    assert(board_image);
    records_button_image = loadImage("data/high_scores_button.png");
    assert(records_button_image);
    records_button_rect = {(PADDING * 2) + TILE_SIZE, (PADDING * 4) + (TILE_SIZE * 3) + 8, TILE_SIZE, 48};
    records_table_image = loadImage("data/records_table.png");
    assert(records_table_image);
    digits[0] = loadImage("data/0.png");
    digits[1] = loadImage("data/1.png");
    digits[2] = loadImage("data/2.png");
    digits[3] = loadImage("data/3.png");
    digits[4] = loadImage("data/4.png");
    digits[5] = loadImage("data/5.png");
    digits[6] = loadImage("data/6.png");
    digits[7] = loadImage("data/7.png");
    digits[8] = loadImage("data/8.png");
    digits[9] = loadImage("data/9.png");
    assert(digits[0]);
    assert(digits[1]);
    assert(digits[2]);
    assert(digits[3]);
    assert(digits[4]);
    assert(digits[5]);
    assert(digits[6]);
    assert(digits[7]);
    assert(digits[8]);
    assert(digits[9]);
    
    screen = SDL_GetVideoSurface();
    assert(screen);
    
    srand(time(0));
    
    // read records file
    ifstream records_file("records");
    if (records_file.good()) {
        records_file.read((char*) &record_wins, sizeof(int));
        records_file.read((char*) &record_losses, sizeof(int));
        records_file.read((char*) &record_ties, sizeof(int));
    } else {
        record_wins = 0;
        record_losses = 0;
        record_ties = 0;
    }
    records_file.close();
    
    winner = 0;
}

void PlayState::finish() {
    ofstream records_file("records");
    if (records_file.good()) {
        records_file.write((char*) &record_wins, sizeof(int));
        records_file.write((char*) &record_losses, sizeof(int));
        records_file.write((char*) &record_ties, sizeof(int));
    }
    records_file.close();
}

void PlayState::update() {
    
    SDL_Event event;
    
    struct Point {
        int x;
        int y;
    };
    
    Point clicked = {-1, -1};
    
    // handle events
    while (SDL_PollEvent(&event)) {
        if (event.type == SDL_MOUSEBUTTONDOWN) {
            if (event.button.button == SDL_BUTTON_LEFT) {
                
                if (winner > 0) {
                    
                    for (int r = 0; r < 3; r++) {
                        for (int c = 0; c < 3; c++) {
                            board[r][c] = 0;
                        }
                    }
                    
                    if (winner == 1) {
                        record_wins++;
                    } else if (winner == 2) {
                        record_losses++;
                    } else if (winner == 3) {
                        record_ties++;
                    }
                    
                    winner = 0;
                } else if (winner < 0) {
                    winner = 0;
                } else {
                    // check for clicks in one of the buttons
                    
                    // records button
                    if (event.button.x > records_button_rect.x && event.button.y > records_button_rect.y && event.button.x < records_button_rect.x + records_button_rect.w && event.button.y < records_button_rect.y + records_button_rect.h) {
                        winner = -1;
                    } else {
                    
                        // check for clicks on a tile
                        SDL_Rect where = {PADDING, PADDING, TILE_SIZE, TILE_SIZE};
                
                        for (int r = 0; r < 3; r++) {
                            for (int c = 0; c < 3; c++) {
                                if (event.button.x > where.x && event.button.x < where.w && event.button.y > where.y && event.button.y < where.h) {
                                    clicked = {r, c};
                                }
                                where.x += TILE_SIZE + PADDING;
                                where.w += TILE_SIZE + PADDING;
                            }
                            where.y += TILE_SIZE + PADDING;
                            where.h += TILE_SIZE + PADDING;
                            where.x = PADDING;
                            where.w = TILE_SIZE;
                        }
                    }
                }
            }
        } else if (event.type == SDL_QUIT) {
            sm->transition(0);
            return;
        }
    }
    
    if (clicked.x != -1) {
        // player move
        board[clicked.x][clicked.y] = 1;
        
        winner = checkWinner();
        
        if (winner == 0){
            opponentMove();
            winner = checkWinner();
        }
    }
}

void PlayState::opponentMove() {
    struct Point {
        int x;
        int y;
    };
    
    vector<Point> possible_moves;
    
    for (int r = 0; r < 3; r++) {
        for (int c = 0; c < 3; c++) {
            if (board[r][c] == 0) {
                possible_moves.push_back({r, c});
            }
        }
    }
    
    int choice = rand() % possible_moves.size();
    
    board[possible_moves[choice].x][possible_moves[choice].y] = 2;
}

int PlayState::checkWinner() {
        
    // check rows
    for (int r = 0; r < 3; r++) {
        if (board[r][0] == board[r][1] && board[r][1] == board[r][2]) {
            if (board[r][0] != 0) {
                return board[r][0];
            }
        }
    }
        
    // check columns
    for (int c = 0; c < 3; c++) {
        if (board[0][c] == board[1][c] && board[1][c] == board[2][c]) {
            if (board[0][c] != 0) {
                return board[0][c];
            }
        }
    }
        
    if (board[0][0] == board[1][1] && board[1][1] == board[2][2]) {
        return board[1][1];
    }
    if (board[0][2] == board[1][1] && board[1][1] == board[2][0]) {
        return board[1][1];
    }
    
    // check for ties
    bool tie = true;
    for (int r = 0; r < 3; r++) {
        for (int c = 0; c < 3; c++) {
            if (board[r][c] == 0) {
                tie = false;
                break;
            }
        }
    }
    
    if (tie) {
        return 3;
    }
    
    return 0;
}

void PlayState::render() {
    
    static int next_frame = 0;
    
    SDL_BlitSurface(board_image, NULL, screen, NULL);
    
    SDL_Rect where = {PADDING, PADDING, TILE_SIZE, TILE_SIZE};
    for (int r = 0; r < 3; r++) {
        for (int c = 0; c < 3; c++) {
            if (board[r][c] == 0) {
                SDL_BlitSurface(blank, NULL, screen, &where);
            } else if (board[r][c] == 1) {
                SDL_BlitSurface(x, NULL, screen, &where);
            } else if (board[r][c] == 2) {
                SDL_BlitSurface(o, NULL, screen, &where);
            }
            where.x += TILE_SIZE + PADDING;
            where.w += TILE_SIZE + PADDING;
        }
        where.y += TILE_SIZE + PADDING;
        where.h += TILE_SIZE + PADDING;
        where.x = PADDING;
        where.w = TILE_SIZE;
    }
    
    SDL_BlitSurface(records_button_image, NULL, screen, &records_button_rect);
    
    if (winner == -1) {
        SDL_Rect where = {PADDING + (TILE_SIZE / 2), PADDING + (TILE_SIZE / 2), 288, 288};
        SDL_BlitSurface(records_table_image, NULL, screen, &where);
        // now blit numbers
        
        // win record number
        where = {static_cast<Sint16>(where.x + 288 - 48), static_cast<Sint16>(where.y + 16), 32, 96};
        SDL_BlitSurface(digits[record_wins % 10], NULL, screen, &where);
        if (record_wins >= 10) {
            where.x -= 32;
            SDL_BlitSurface(digits[(record_wins % 100) / 10], NULL, screen, &where);
            if (record_wins >= 100) {
                where.x -= 32;
                SDL_BlitSurface(digits[(record_wins % 1000) / 100], NULL, screen, &where);
            }
        }
        
        // losses
        where.x = (PADDING + (TILE_SIZE / 2)) + 288 - 48;
        where.y += 96;
        SDL_BlitSurface(digits[record_losses % 10], NULL, screen, &where);
        if (record_losses >= 10) {
            where.x -= 32;
            SDL_BlitSurface(digits[(record_losses % 100) / 10], NULL, screen, &where);
            if (record_losses >= 100) {
                where.x -= 32;
                SDL_BlitSurface(digits[(record_losses % 1000) / 100], NULL, screen, &where);
            }
        }
        
        // ties
        where.x = (PADDING + (TILE_SIZE / 2)) + 288 - 48;
        where.y += 96;
        SDL_BlitSurface(digits[record_ties % 10], NULL, screen, &where);
        if (record_ties >= 10) {
            where.x -= 32;
            SDL_BlitSurface(digits[(record_ties % 100) / 10], NULL, screen, &where);
            if (record_ties >= 100) {
                where.x -= 32;
                SDL_BlitSurface(digits[(record_ties % 1000) / 100], NULL, screen, &where);
            }
        }
        
    } if (winner == 1) {
        SDL_Rect where = {PADDING + (TILE_SIZE / 2), PADDING + TILE_SIZE, 288, 160};
        SDL_BlitSurface(win_image, NULL, screen, &where);
    } else if (winner == 2) {
        SDL_Rect where = {PADDING + (TILE_SIZE / 2), PADDING + TILE_SIZE, 288, 160};
        SDL_BlitSurface(lose_image, NULL, screen, &where);
    } else if (winner == 3) {
        SDL_Rect where = {PADDING + (TILE_SIZE / 2), PADDING + TILE_SIZE, 288, 160};
        SDL_BlitSurface(tie_image, NULL, screen, &where);
    }
    
    SDL_Flip(screen);

	// Regulate frame rate.
    Uint32 now = SDL_GetTicks();
    if (next_frame <= now) {
        next_frame = now + (1000 / 60);
    } else {
        SDL_Delay(next_frame - now);
    }
}