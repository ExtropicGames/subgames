#ifndef PLAY_STATE_H
#define PLAY_STATE_H

#include <SDL/SDL.h>

#include "StateMachine.h"

const int TILE_SIZE = 128;
const int PADDING = 16;
const int GUTTER = 64;

class PlayState : public State {
public:
    int id() { return 1; }
    
    void start();
    void finish();
    void update();
    void render();
private:
    int checkWinner();
    void opponentMove();
    
    int board[3][3];
    int winner;
    
    int record_wins;
    int record_losses;
    int record_ties;
    
    SDL_Surface* screen;
    SDL_Surface* board_image;
    SDL_Surface* blank;
    SDL_Surface* x;
    SDL_Surface* o;
    SDL_Surface* win_image;
    SDL_Surface* lose_image;
    SDL_Surface* tie_image;
    SDL_Surface* records_button_image;
    SDL_Rect records_button_rect;
    SDL_Surface* records_table_image;
    SDL_Surface* digits[10];
};

#endif