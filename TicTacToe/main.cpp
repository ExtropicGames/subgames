#include <iostream>

#include <cassert>

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include "PlayState.h"

using namespace std;

int checkWinner();

int main(int argc, char** argv) {

    atexit(SDL_Quit);
    if (SDL_Init(SDL_INIT_EVERYTHING) == -1) {
        cout << "can't init" << endl;
        exit(1);
    }
    
    int screen_size = (PADDING * 4) + (TILE_SIZE * 3);
    
    SDL_Surface* screen = SDL_SetVideoMode(screen_size, screen_size + GUTTER, 32, SDL_HWSURFACE|SDL_DOUBLEBUF);
    
    assert(screen);
    
    SDL_WM_SetCaption("Tic Tac Toe", NULL);
    
    StateMachine sm;
    sm.add(make_shared<QuitState>());
    sm.add(make_shared<PlayState>());
    
    sm.start(1);
    
    while (!sm.quit()) {
        sm.update();
        sm.render();
    }

    return 0;
}