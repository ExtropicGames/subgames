#ifndef STORY_STATE_H
#define STORY_STATE_H

#ifdef WIN32
#include <SDL.h>
#else
#include <SDL/SDL.h>
#endif

#include "StateMachine.h"

class StoryState : public State {
public:
    int id() { return 3; }
    
    void start();
    void finish();
    void update();
    void render();
private:
    SDL_Surface* screen;
    
    SDL_Surface* alphabet_image;
    
    static const int FRAMES_PER_SECOND = 20;
};

#endif