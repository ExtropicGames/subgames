#include "StoryState.h"

#include "sdlfunc.h"
#include "globals.h"

void StoryState::start() {
        
    screen = SDL_GetVideoSurface();
    
    alphabet_image = loadImage("data/alphabet.png");
}

void StoryState::finish() {
    SDL_FreeSurface(alphabet_image);
}

void StoryState::update() {
    
    SDL_Event event;
    
    while (SDL_PollEvent(&event)) {
        if (event.type == SDL_KEYDOWN) {
            if (event.key.keysym.sym == SDLK_ESCAPE) {
                sm->transition(0);
            } else {
                sm->transition(1);
            }
        } else if (event.type == SDL_QUIT) {
            sm->transition(0);
        }
    }
    
}

void StoryState::render() {
    
    static Uint32 next_frame = 0;
    
    
    SDL_Flip(screen);

	// Regulate frame rate.
    Uint32 now = SDL_GetTicks();
    if (next_frame <= now) {
        next_frame = now + (1000 / FRAMES_PER_SECOND);
    } else {
        SDL_Delay(next_frame - now);
    }
}