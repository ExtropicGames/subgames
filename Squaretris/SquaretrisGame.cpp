#include <cassert>
#include <cstdlib>

#include "SquaretrisGame.h"

using namespace std;

void SquaretrisGame::reset() {
    for (int x = 0; x < BOARD_WIDTH; x++) {
        for (int y = 0; y < BOARD_HEIGHT; y++) {
            board[x][y] = 0;
        }
    }

    next_piece = rand() % 7;
    current_piece = rand() % 7;
    piece_rotation = 0;
    piece_position_x = 4;
    piece_position_y = 0;
    score = 0;
    state = NORMAL;
    level = 0;
	drop_timer = level_drop_timers[level];
    lock_timer = level_lock_timers[level];
}

void SquaretrisGame::moveLeft() {
    piece_position_x--;
    if (!valid()) {
        piece_position_x++;
    }
    
    //lock_timer = level_lock_timers[level];
}

void SquaretrisGame::moveRight() {
    piece_position_x++;
    if (!valid()) {
        piece_position_x--;
        return;
    }
    
    //lock_timer = level_lock_timers[level];
}

void SquaretrisGame::rotate() {
    piece_rotation++;
    piece_rotation %= 4;
    if (!valid()) {
        // try kicking right
        piece_position_x++;
        if (!valid()) {
            // try kicking left
            piece_position_x -= 2;
            if (!valid()) {
                // try a ceiling kick
                if (current_piece == 0) {
                    piece_position_y++;
                    if (valid()) {
                        return;
                    }
                    piece_position_y--;
                }
                
                // can't legally rotate, return to initial position
                piece_position_x++;
                piece_rotation--;
                if (piece_rotation == -1) {
                    piece_rotation = 3;
                }
                piece_rotation %= 4;
                return;
            }
        }
    }
    
    // uncomment this to reset lock timers on rotation
    //lock_timer = level_lock_timers[level];
}

void SquaretrisGame::drop() {
    lock_timer = 0;
    while (!fall()) {
        lock_timer = 0;
    }
}

// returns true if piece locked
bool SquaretrisGame::fall() {
    
    piece_position_y++;
    drop_timer = level_drop_timers[level];
    
    // if it hits the bottom, copy it to the grid
    if (!valid()) {
        piece_position_y--;
        
        // check lock delay
        if (lock_timer <= 0) {
            // lock piece to board
            for (int i = 0; i < 16; i++) {
                int x = piece_position_x + (i % 4);
                int y = piece_position_y + (i / 4);
            
                if (tetrominos[current_piece][piece_rotation] & (1<<i)) {
                    board[x][y] = current_piece + 1;
                }
            }
            // clear rows
            clearRows();
            
            // set next piece
            current_piece = next_piece;
            next_piece = rand() % 7;
            piece_rotation = 0;
            lock_timer = level_lock_timers[level];
            drop_timer = level_drop_timers[level];
            if (current_piece == 0) {
                piece_position_x = 3;
                piece_position_y = -1;
            } else {
                piece_position_x = 4;
                piece_position_y = 0;
            }

            // check for losing
            if (!valid()) {
                // game over bro
                state = LOST;
                if (isHighscore()) {
                    state = LOST_HIGHSCORE;
                }
            }
            return true;
        }
        return false;
    }
    return false;
}

bool SquaretrisGame::pieceOnBottom() {
    piece_position_y++;
    
    if (valid()) {
        piece_position_y--;
        return false;
    } else {
        piece_position_y--;
        return true;
    }
}

void SquaretrisGame::clearRows() {
    int clear_count = 0;
   
    for (int y = 0; y < BOARD_HEIGHT; y++) {
        bool clear = true;
        for (int x = 0; x < BOARD_WIDTH; x++) {
            if (board[x][y] == 0) {
                clear = false;
            }
        }
        if (clear == true) {
            clear_count++;
            for (int i = y; i > 0; i--) {
                for (int x = 0; x < BOARD_WIDTH; x++) {
                    board[x][i] = board[x][i-1];
                }
            }
        }
    }

	// get bonus points if entire board is cleared
	bool all_clear = true;
	for (int x = 0; x < BOARD_WIDTH; x++) {
		for (int y = 0; y < BOARD_HEIGHT; y++) {
			if (board[x][y] > 0) {
				all_clear = false;
				break;
			}
		}
		if (!all_clear) {
			break;
		}
	}
	if (all_clear) {
		score += (100 * level);
	}
    
	// add up number of cleared rows and give points
    if (clear_count > 0) {
        for (int i = 1; i <= clear_count; i++) {
            score += ((5 * (level+1)) << i);
        }
    }

	if (score > next_level_score[level]) {
		level++;
		if (level == 10 && achievements[BEAT_LEVELS] == LOCKED) {
			achievement_unlocked = true;
			achievements[BEAT_LEVELS] = NEWLY_UNLOCKED;
		}
        lock_timer = level_lock_timers[level];
		drop_timer = level_drop_timers[level];
	}
}

bool SquaretrisGame::isHighscore() {
    for (int i = 0; i < HIGH_SCORE_LIST_LENGTH; i++) {
        if (score > high_scores[i]) {
            return true;
        }
    }
    return false;
}

void SquaretrisGame::addHighscore(const char* name) {
    // add score to high scores list if necessary
    for (int i = 0; i < HIGH_SCORE_LIST_LENGTH; i++) {
        if (high_scores[i] < score) {
            state = LOST_HIGHSCORE;
            for (int m = HIGH_SCORE_LIST_LENGTH - 1; m > i; m--) {
                high_scores[m] = high_scores[m-1];
                strncpy(high_score_names[m], high_score_names[m-1], 9);
            }
            high_scores[i] = score;
            strncpy(high_score_names[i], name, 9);
            break;
        }
    }
}

bool SquaretrisGame::valid() {
    for (int i = 0; i < 16; i++) {

        int x = piece_position_x + (i % 4);
        int y = piece_position_y + (i / 4);
        
        if (tetrominos[current_piece][piece_rotation] & (1<<i)) {
            
            if (x < 0 || x >= BOARD_WIDTH || y < 0 || y >= BOARD_HEIGHT) {
                return false;
            }
            
            if (board[x][y] != 0) {
                return false;
            }
        }
    }
    
    return true;
}