#include <ctime>
#include <cstdlib>

#include "SplashState.h"

#include "sdlfunc.h"
#include "globals.h"

void SplashState::start() {

    srand((unsigned int) time(0));

    screen = SDL_GetVideoSurface();

    splash_image = loadImage("data/splash.png");
    splash_logo_image = loadImage("data/splash_logo.png");
    splash_background_image = loadImage("data/splash_background.png");
    splash_text_image = loadImage("data/splash_text.png");

    slide_position_x = 0;
    slide_position_y = 0;
    slide_velocity_x = 1;
    slide_velocity_y = 1;
    prev_color = 0;
    next_color = 0;
    color_state = 0;

    music = Mix_LoadMUS("data/music/RejectedExecutionHandler.wav");
    if (!music) {
      printf("failed to load music %s\n", Mix_GetError());
    }
    Mix_PlayMusic(music, -1); // -1 is loop indefinitely
}

void SplashState::finish() {
    SDL_FreeSurface(splash_image);
    SDL_FreeSurface(splash_logo_image);
    SDL_FreeSurface(splash_background_image);
    Mix_HaltMusic();
    Mix_FreeMusic(music);
}

void SplashState::update() {

    SDL_Event event;

    while (SDL_PollEvent(&event)) {
        if (event.type == SDL_KEYDOWN) {
            if (event.key.keysym.sym == SDLK_ESCAPE) {
                sm->transition(0);
            } else {
                sm->transition(1);
            }
        } else if (event.type == SDL_QUIT) {
            sm->transition(0);
        }
    }

    slide_position_x += slide_velocity_x;
    slide_position_y += slide_velocity_y;
}

void SplashState::render() {

    static Uint32 next_frame = 0;

    SDL_BlitSurface(splash_image, NULL, screen, NULL);

    for (short x = 0 - slide_position_x; x < SCREEN_WIDTH; x += 128) {
        for (short y = 0 - slide_position_y; y < SCREEN_HEIGHT; y += 128) {
            SDL_Rect where = {x, y, 128, 128};
            SDL_BlitSurface(splash_background_image, NULL, screen, &where);
        }
    }

    SDL_BlitSurface(splash_logo_image, NULL, screen, NULL);

    paletteSwap(splash_text_image, (SDL_Color*) &prev_color, (SDL_Color*) &next_color);
    prev_color = next_color;

    rainbow();

    SDL_Rect text_where = {202, 432, 0,0};
    SDL_BlitSurface(splash_text_image, NULL, screen, &text_where);

    SDL_Flip(screen);

	// Regulate frame rate.
    Uint32 now = SDL_GetTicks();
    if (next_frame <= now) {
        next_frame = now + (1000 / FRAMES_PER_SECOND);
    } else {
        SDL_Delay(next_frame - now);
    }
}

void SplashState::rainbow() {
    // max red
    if (color_state == 0) {
        next_color += 0x05;
        if ((next_color & 0xFF) == 0xFF) {
            color_state = 1;
        }
    // max green
    } else if (color_state == 1) {
        next_color += 0x0500;
        if ((next_color & 0xFF00) == 0xFF00) {
            color_state = 2;
        }
    // min red
    } else if (color_state == 2) {
        next_color -= 0x05;
        if ((next_color & 0xFF) == 0x00) {
            color_state = 3;
        }
    // max blue
    } else if (color_state == 3) {
        next_color += 0x050000;
        if ((next_color & 0xFF0000) == 0xFF0000) {
            color_state = 4;
        }
    // min green
    } else if (color_state == 4) {
        next_color -= 0x0500;
        if ((next_color & 0xFF00) == 0x0000) {
            color_state = 5;
        }
    // max red
    } else if (color_state == 5) {
        next_color += 0x05;
        if ((next_color & 0xFF) == 0xFF) {
            color_state = 6;
        }
    // max green
    } else if (color_state == 6) {
        next_color += 0x0500;
        if ((next_color & 0xFF00) == 0xFF00) {
            color_state = 7;
        }
    } else if (color_state == 7) {
        next_color -= 0x050505;
        if (next_color == 0) {
            color_state = 0;
        }
    }
}
