#include <iostream>

#include <cassert>

#ifdef WIN32
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#else
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_mixer.h>
#endif

#include "PlayState.h"
#include "SplashState.h"
//#include "StoryState.h"

#include "globals.h"

using namespace std;

int checkWinner();

int main(int argc, char** argv) {

    atexit(SDL_Quit);
    if (SDL_Init(SDL_INIT_EVERYTHING) == -1) {
        cout << "can't init" << endl;
        exit(1);
    }

    SDL_Surface* screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32, SDL_HWSURFACE|SDL_DOUBLEBUF);

    assert(screen);

	atexit(Mix_CloseAudio);
	// Set up SDL_mixer
	if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, AUDIO_S16, 2, 4096)) {
		cout << "can't init audio" << endl;
		exit(1);
	}

    SDL_WM_SetCaption("Squaretris", NULL);

    SDL_EnableKeyRepeat(200, 10);

    StateMachine sm;
    sm.add(make_shared<QuitState>());
    sm.add(make_shared<PlayState>());
    sm.add(make_shared<SplashState>());
//    sm.add(make_shared<StoryState>());

    sm.start(2);

    while (!sm.quit()) {
        sm.update();
        sm.render();
    }

    return 0;
}
