#include <vector>
#include <fstream>
#include <iostream>

#include <cstdlib>
#include <ctime>

#ifdef WIN32
#include <SDL.h>
#else
#include <SDL/SDL.h>
#endif

#include "PlayState.h"
#include "sdlfunc.h"

using namespace std;

void PlayState::start() {
    
    cheats_enabled = false;
    konami_state = 0;
	fall_key_pressed = false;
    
    // load images
    screen = SDL_GetVideoSurface();
    block_images[0] = loadImage("data/red.png");
    block_images[1] = loadImage("data/green.png");
    block_images[2] = loadImage("data/gray.png");
    block_images[3] = loadImage("data/purple.png");
    block_images[4] = loadImage("data/blue.png");
    block_images[5] = loadImage("data/cyan.png");
    block_images[6] = loadImage("data/yellow.png");
    board_image = loadImage("data/board.png");
    numbers_image = loadImage("data/numbers.png");
    pause_image = loadImage("data/paused.png");
    alphabet_image = loadImage("data/alphabet.png");
    high_score_image = loadImage("data/highscore.png");
    lose_image = loadImage("data/lose.png");
    level_icons[0] = loadImage("data/earth_icon.png");
    level_icons[1] = loadImage("data/earth_icon.png"); // should be mars
    level_icons[2] = loadImage("data/venus_icon.png");
    level_icons[3] = loadImage("data/mercury_icon.png");
    level_icons[4] = loadImage("data/mercury_icon.png"); // should be the asteroid belt
    level_icons[5] = loadImage("data/jupiter_icon.png");
    level_icons[6] = loadImage("data/jupiter_icon.png"); // should be saturn
    level_icons[7] = loadImage("data/jupiter_icon.png"); // should be neptune
    level_icons[8] = loadImage("data/jupiter_icon.png"); // should be uranus
    level_icons[9] = loadImage("data/jupiter_icon.png"); // should be the sun
    level_icons[10] = loadImage("data/jupiter_icon.png"); // should be infinity

    level_backgrounds[0] = loadImage("data/earth_background.png");
    level_backgrounds[1] = loadImage("data/mars_background.png");
    level_backgrounds[2] = loadImage("data/venus_background.png");
    level_backgrounds[3] = loadImage("data/mercury_background.png");
    level_backgrounds[4] = loadImage("data/mercury_background.png");
    level_backgrounds[5] = loadImage("data/jupiter_background.png");
    level_backgrounds[6] = loadImage("data/jupiter_background.png");
    level_backgrounds[7] = loadImage("data/jupiter_background.png");
    level_backgrounds[8] = loadImage("data/jupiter_background.png");
    level_backgrounds[9] = loadImage("data/jupiter_background.png");
    level_backgrounds[10] = loadImage("data/jupiter_background.png");
    
    for (int i = 0; i < 7; i++) {
        assert(block_images[i]);
    }
    // assert images were loaded
    assert(screen);
    assert(board_image);
    assert(numbers_image);
    assert(pause_image);
    assert(alphabet_image);
    
    game.reset();
    
    // read high scores file
    ifstream high_scores_file("high_scores");
    if (high_scores_file.good()) {
        high_scores_file.read((char*) &game.high_scores, sizeof(int) * 10);
        high_scores_file.read((char*) &game.high_score_names, sizeof(char) * 10 * 9);
    } else {
        for (int i = 0; i < 10; i++) {
            game.high_scores[i] = 1000 - (i * 100);
            game.high_score_names[i][0] = 'A';
            game.high_score_names[i][1] = 'L';
            game.high_score_names[i][2] = 'E';
            game.high_score_names[i][3] = 'X';
            game.high_score_names[i][4] = 'E';
            game.high_score_names[i][5] = 'Y';
            game.high_score_names[i][6] = '\0';
        }
    }
    high_scores_file.close();

	current_level = 0;
	music[0] = Mix_LoadMUS("data/music/PreDestroy.wav");
	music[1] = Mix_LoadMUS("data/music/NumberOfInterveningJobs.wav");
	music[2] = Mix_LoadMUS("data/music/NamingContextPOA.wav");
	music[3] = Mix_LoadMUS("data/music/ActivationGroup.wav");
	music[4] = Mix_LoadMUS("data/music/PreDestroy.wav");
	music[5] = Mix_LoadMUS("data/music/PreDestroy.wav");
	music[6] = Mix_LoadMUS("data/music/PreDestroy.wav");
	music[7] = Mix_LoadMUS("data/music/PreDestroy.wav");
	music[8] = Mix_LoadMUS("data/music/PreDestroy.wav");
	music[9] = Mix_LoadMUS("data/music/PreDestroy.wav");
	music[10] = Mix_LoadMUS("data/music/UMLtoXMLConverter.wav");
	Mix_PlayMusic(music[0], -1);
}

void PlayState::finish() {
    ofstream high_scores_file("high_scores");
    if (high_scores_file.good()) {
        high_scores_file.write((char*) &game.high_scores, sizeof(int) * 10);
        high_scores_file.write((char*) &game.high_score_names, sizeof(char) * 10 * 9);
    }
    high_scores_file.close();
}

void PlayState::update() {

	if (current_level != game.level) {
		current_level = game.level;
		Mix_HaltMusic();
		Mix_PlayMusic(music[current_level], -1);
	}

    SDL_Event event;
    
    // handle events
    while (SDL_PollEvent(&event)) {
        if (event.type == SDL_KEYDOWN) {
            
            // check for konami code
            if (!cheats_enabled) {
                if (event.key.keysym.sym == konami_code[konami_state]) {
                    if (konami_state == 9) {
                        cheats_enabled = true;
                    } else {
                        konami_state++;
                    }
                } else {
                    konami_state = 0;
                }
            }
            
            // Handle events after losing
            if (game.state == LOST) {
                game.reset();
            }
            
            // Handle events while paused
            else if (game.state == PAUSED) {
                if (event.key.keysym.sym == SDLK_p) {
                    game.state = NORMAL;
                }
            } 
            
            // Handle events if you got a high score
            else if (game.state == LOST_HIGHSCORE) {
                
                if (event.key.keysym.sym == SDLK_BACKSPACE) {
                    if (nameEntry.length() > 0) {
                        nameEntry.erase(nameEntry.end() - 1);
                    }
                } else if (event.key.keysym.sym >= SDLK_a && event.key.keysym.sym <= SDLK_z) {
                    if (nameEntry.length() < 8) {
                        char c = (event.key.keysym.sym - SDLK_a) + 'A';
                        nameEntry.append(1, c);
                    }
                } else if (event.key.keysym.sym == SDLK_RETURN) {
                    game.addHighscore(nameEntry.c_str());
                    game.reset();
                }
            }
            
            // Handle other events
            else if (game.state == NORMAL) {
                if (event.key.keysym.sym == SDLK_LEFT) {
                    game.moveLeft();
                } else if (event.key.keysym.sym == SDLK_RIGHT) {
                    game.moveRight();
                } else if (event.key.keysym.sym == SDLK_SPACE) {
                    game.drop();
                } else if (event.key.keysym.sym == SDLK_DOWN) {
					fall_key_pressed = true;
                } else if (event.key.keysym.sym == SDLK_UP) {
                    game.rotate();
                } else if (event.key.keysym.sym == SDLK_p) {
                    game.state = PAUSED;
                }
            } 
            
            // Escape key quits from any state.
            if (event.key.keysym.sym == SDLK_ESCAPE) {
                sm->transition(0);
            } else if (event.key.keysym.sym == SDLK_s) {
                if (cheats_enabled) {
                    game.score += 100;
                }
            }
		} else if (event.type == SDL_KEYUP) {
			if (event.key.keysym.sym == SDLK_DOWN) {
				fall_key_pressed = false;
			}
        } else if (event.type == SDL_QUIT) {
            sm->transition(0);
            return;
        }
    }
    
    // let block fall
	if (fall_key_pressed) {
		game.fall();
	} else {
		if (game.drop_timer <= 0) {
			game.fall();
		}
	}
	
	if (game.state == NORMAL){
	    if (game.pieceOnBottom()) {
            game.lock_timer--;
            game.fall();
        }
        game.drop_timer--;
    }
}

void PlayState::render() {
    
    static Uint32 next_frame = 0;
    
    SDL_BlitSurface(level_backgrounds[game.level], NULL, screen, NULL);

    // render board
    SDL_BlitSurface(board_image, NULL, screen, NULL);
    SDL_Rect sprite_clip = {128,0,32,32};

    // render blocks on board
    SDL_Rect where = {BOARD_OFFSET_X, BOARD_OFFSET_Y, BOARD_OFFSET_X + BLOCK_SIZE, BOARD_OFFSET_Y + BLOCK_SIZE};
    for (int r = 0; r < BOARD_WIDTH; r++) {
        for (int c = 0; c < BOARD_HEIGHT; c++) {
            if (game.board[r][c] != 0) {
                assert(game.board[r][c] < 8);

                SDL_BlitSurface(block_images[game.board[r][c] - 1], &sprite_clip, screen, &where);
            }
            where.y += BLOCK_SIZE;
            where.h += BLOCK_SIZE;
        }
        where.x += BLOCK_SIZE;
        where.w += BLOCK_SIZE;
        where.y = BOARD_OFFSET_Y;
        where.h = BOARD_OFFSET_Y + BLOCK_SIZE;
    }

    // render current piece
    where.x = BOARD_OFFSET_X + (BLOCK_SIZE * game.piece_position_x);
    where.y = BOARD_OFFSET_Y + (BLOCK_SIZE * game.piece_position_y);

    if (game.pieceOnBottom()) {
        sprite_clip.x = 32 * (3 - (game.lock_timer * 3) / (level_lock_timers[game.level]));
    } else {
        sprite_clip.x = 0;
    }
    
    for (int i = 0; i < 16; i++) {
        SDL_Rect block_where;
        block_where.x = where.x + ((i % 4) * BLOCK_SIZE);
        block_where.y = where.y + ((i / 4) * BLOCK_SIZE);
        block_where.w = block_where.x + BLOCK_SIZE;
        block_where.h = block_where.y + BLOCK_SIZE;
        if (tetrominos[game.current_piece][game.piece_rotation] & (1<<i)) {
            assert(game.current_piece < 7);
            SDL_BlitSurface(block_images[game.current_piece], &sprite_clip, screen, &block_where);
        }
    }

    sprite_clip.x = 0;

    // render next piece
    for (int i = 0; i < 16; i++) {
        SDL_Rect block_where;
        block_where.y = NEXT_PIECE_OFFSET_Y + ((i / 4) * BLOCK_SIZE);
        if (game.next_piece == 0 || game.next_piece == 1) {
            block_where.x = NEXT_PIECE_OFFSET_X + ((i % 4) * BLOCK_SIZE);
        } else {
            block_where.x = NEXT_PIECE_OFFSET_X + ((i % 4) * BLOCK_SIZE) + (BLOCK_SIZE / 2);
        }
        block_where.w = block_where.x + BLOCK_SIZE;
        block_where.h = block_where.y + BLOCK_SIZE;
        if (tetrominos[game.next_piece][0] & (1<<i)) {
            SDL_BlitSurface(block_images[game.next_piece], &sprite_clip, screen, &block_where);
        }
    }

    // render current score    
    where.x = SCORE_OFFSET_X;
    where.y = SCORE_OFFSET_Y;
    drawNumber(game.score, &where);

    // render high scores
    for (int i = 0; i < 10; i++) {
        where.x = HIGH_SCORE_OFFSET_X;
        where.y = HIGH_SCORE_OFFSET_Y + 16 + (12 * i);
        SDL_Rect name_offset;
        name_offset.x = where.x - 138;
        name_offset.y = where.y;
        drawNumber(game.high_scores[i], &where);
        drawString(game.high_score_names[i], &name_offset);
    }
    
    // render level icon
    where.x = LEVEL_ICON_X;
    where.y = LEVEL_ICON_Y;
    SDL_BlitSurface(level_icons[game.level], NULL, screen, &where);

    // show modal boxes for different game states
    if (game.state == PAUSED) {
		where.x = BOARD_OFFSET_X + 64;
		where.y = BOARD_OFFSET_Y + 160;
        //where = {BOARD_OFFSET_X + 64, BOARD_OFFSET_Y + 160, BOARD_WIDTH * BLOCK_SIZE, (BOARD_WIDTH * BLOCK_SIZE / 2)};
        SDL_BlitSurface(pause_image, NULL, screen, &where);
    } else if (game.state == LOST) {
        where.x = BOARD_OFFSET_X + 49;
		where.y = BOARD_OFFSET_Y + 160;
        SDL_BlitSurface(lose_image, NULL, screen, &where);
    } else if (game.state == LOST_HIGHSCORE) {
        where.x = BOARD_OFFSET_X + 61;
		where.y = BOARD_OFFSET_Y + 160;
        SDL_BlitSurface(high_score_image, NULL, screen, &where);
        where.x += 54;
        where.y += 56;
        drawString(nameEntry.c_str(), &where);
    }

    where.x = 220;
    where.y = 11;
    drawString("SQUARETRIS", &where);

    if (cheats_enabled) {
        where.x = 220;
        where.y = 683;
        drawString("CHEATER!!!", &where);
    }
    
    SDL_Flip(screen);

	// Regulate frame rate.
    Uint32 now = SDL_GetTicks();
    if (next_frame <= now) {
        next_frame = now + (1000 / FRAMES_PER_SECOND);
    } else {
        SDL_Delay(next_frame - now);
    }
}

void PlayState::drawNumber(unsigned int number, SDL_Rect* where) {
    int digit = number % 10;
    SDL_Rect number_location = {0,0,6,10};
    where->x -= 4;
    where->w = 6;
    where->h = 10;
    
    do {
        digit = number % 10;
        number /= 10;
        number_location.x = 6 * digit;
        assert(number_location.x >= 0);
        assert(number_location.x < 55);
        if (digit == 1) {
            where->x += 2;
        }
        SDL_BlitSurface(numbers_image, &number_location, screen, where);
        if (digit == 1) {
            where->x -= 6;
        } else {
            where->x -= 8;
        }
    } while (number > 0);
}

// Can only handle uppercase strings.
void PlayState::drawString(const char* s, SDL_Rect* where) {
    int i = 0;
    SDL_Rect char_location = {0,0,10,10};
    where->w = 10;
    where->h = 10;
    
    while (s[i] != '\0') {
        char_location.x = ((s[i] - 'A') * 10) + 10;
        
        // special casing for non-alphabet characters for now
        if (s[i] == '!') {
            char_location.x = 0;
        }
        SDL_BlitSurface(alphabet_image, &char_location, screen, where);
        where->x += 12;
        i++;
    }
}