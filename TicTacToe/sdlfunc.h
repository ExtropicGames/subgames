#include <cassert>

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

SDL_Surface* loadImage(const char* filename) {
    SDL_Surface* unopt = IMG_Load(filename);
    assert(unopt);
    SDL_Surface* result = SDL_DisplayFormatAlpha(unopt);
    assert(result);
    SDL_FreeSurface(unopt);
    return result;
}