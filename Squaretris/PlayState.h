#ifndef PLAY_STATE_H
#define PLAY_STATE_H

#ifdef WIN32
#include <SDL.h>
#include <SDL_mixer.h>
#else
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>
#endif

#include "StateMachine.h"
#include "SquaretrisGame.h"

struct point {
    int x;
    int y;
};

const int BOARD_OFFSET_X = 32;
const int BOARD_OFFSET_Y = 32;
const int NEXT_PIECE_OFFSET_X = 32 + 320 + 32 + 8;
const int NEXT_PIECE_OFFSET_Y = 32 + 8 + 32;
const int SCORE_OFFSET_X = 524;
const int SCORE_OFFSET_Y = 220;
const int HIGH_SCORE_OFFSET_X = 524;
const int HIGH_SCORE_OFFSET_Y = 374;
const int LEVEL_ICON_X = 416;
const int LEVEL_ICON_Y = 270;
const int BLOCK_SIZE = 32;
const int FRAMES_PER_SECOND = 20;

const SDLKey konami_code[10] = {SDLK_UP, SDLK_UP, SDLK_DOWN, SDLK_DOWN, SDLK_LEFT, SDLK_RIGHT, SDLK_LEFT, SDLK_RIGHT, SDLK_b, SDLK_a};

class PlayState : public State {
public:
    int id() { return 1; }
    
    void start();
    void finish();
    void update();
    void render();
private:
    void drawNumber(unsigned int number, SDL_Rect* where);
    void drawString(const char* s, SDL_Rect* where);
    
    SquaretrisGame game;
    
    SDL_Surface* screen;
    
    SDL_Surface* block_images[7];
    SDL_Surface* board_image;
    SDL_Surface* numbers_image;
    SDL_Surface* pause_image;
    SDL_Surface* alphabet_image;
    SDL_Surface* high_score_image;
    SDL_Surface* lose_image;
    SDL_Surface* level_icons[11];
    SDL_Surface* level_backgrounds[11];

	Mix_Music* music[11];
	int current_level;
    
    std::string nameEntry;
    bool cheats_enabled;
    int konami_state;
    bool fall_key_pressed;
};

#endif